FROM alpine:3.12

RUN set -euxo pipefail; \
    apk update; \
    apk add --no-cache \
        ca-certificates \
        curl \
        git \
        make \
        wget;

# Install kubectl
ENV KUBECTL_VERSION 1.23.0
RUN set -euxo pipefail; \
    curl --show-error --location --output /tmp/kubectl "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl"; \
    install -o root -g root -m 0755 /tmp/kubectl /usr/bin/kubectl; \
    rm -f /tmp/kubectl

# Install Helm
ENV HELM_VERSION 3.7.2
RUN set -euxo pipefail; \
    mkdir --parents /tmp/helm; \
    curl --show-error --location "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar --extract --gzip --directory /tmp/helm; \
    install -o root -g root -m 0755 /tmp/helm/linux-amd64/helm /usr/bin/helm; \
    rm -rf /tmp/helm

ENV HELM_PUSH_VERSION 0.10.1
RUN set -euxo pipefail; \
    helm plugin install --version "v${HELM_PUSH_VERSION}" https://github.com/chartmuseum/helm-push.git
